
Reminder of release process (new tagged versions):

final prep:
- consider running `cargo update` and checking that everything works
- `make lint` and fix anything
- `make fmt` and commit any fixes
- update CHANGELOG and increment version in `fatcat-cli/Cargo.toml`. `make build` to
  update `Cargo.lock`, then commit
- `make test`, `make deb` and ensure build works and is clean
- ensure working directory is *entirely* clean (no "Untracked files" or edits,
  even if unrelated to code changes)

push/tag/etc:
- when project is ready to share on crates.io, do: push to crates.io: `cargo publish`
- create a git tag: `git tag vX.Y.Z -a -s -u bnewbold@archive.org -m "bump version to X.Y.Z"`
- push git and tag: `git push`, `git push --tags`
- `cp ./target/release/fatcat-cli fatcat-cli_X.Y.Z_amd64_linux`
- upload linux binary and deb package, eg: `ia upload ia-fatcat-cli-bin ./target/debian/fatcat-cli_X.Y.Z_amd64.deb fatcat-cli_X.Y.Z_amd64_linux`

homebrew / OS X (TODO):
- pull project
- build, then build package (?)
- upload package (?)
- update homebrew-fatcat repository
