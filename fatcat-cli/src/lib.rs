use anyhow::{anyhow, Context, Result};
use data_encoding::BASE64;
use macaroon::{Caveat, Macaroon};
use std::path::PathBuf;
use std::str::FromStr;

mod api;
mod commands;
mod download;
mod entities;
mod search;
mod specifier;

pub use api::FatcatApiClient;
pub use commands::{
    edit_entity_locally, print_changelog_entries, print_editgroups, print_entity_histories,
    print_search_table, BatchGrouper, BatchOp, ClientStatus,
};
pub use download::{download_batch, download_file, download_release};
pub use entities::{
    entity_model_from_json_str, read_entity_file, ApiEntityModel, ApiModelIdent, ApiModelSer,
    Mutation,
};
pub use search::{crude_search, SearchResults};
pub use specifier::{EditgroupSpecifier, Specifier};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum EntityType {
    Release,
    Work,
    Container,
    Creator,
    File,
    FileSet,
    WebCapture,
}

impl FromStr for EntityType {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "release" | "releases" => Ok(EntityType::Release),
            "work" | "works" => Ok(EntityType::Work),
            "container" | "containers" => Ok(EntityType::Container),
            "creator" | "creators" => Ok(EntityType::Creator),
            "file" | "files" => Ok(EntityType::File),
            "fileset" | "filesets" => Ok(EntityType::FileSet),
            "webcapture" | "webcaptures" => Ok(EntityType::WebCapture),
            _ => Err(anyhow!("invalid entity type : {}", s)),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum SearchEntityType {
    Release,
    Container,
    File,
    Scholar,
    Reference,
    ReferenceIn,
    ReferenceOut,
}

impl FromStr for SearchEntityType {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "release" | "releases" => Ok(SearchEntityType::Release),
            "container" | "containers" => Ok(SearchEntityType::Container),
            "file" | "files" => Ok(SearchEntityType::File),
            "scholar" | "fulltext" => Ok(SearchEntityType::Scholar),
            "ref" | "refs" | "references" => Ok(SearchEntityType::Reference),
            "ref-in" | "refs-in" | "references-in" => Ok(SearchEntityType::ReferenceIn),
            "ref-out" | "refs-out" | "references-out" => Ok(SearchEntityType::ReferenceOut),
            _ => Err(anyhow!("invalid entity type : {}", s)),
        }
    }
}

/// Takes a macaroon token (as base64-encoded string) and tries to parse out an editor id
pub fn parse_macaroon_editor_id(s: &str) -> Result<String> {
    let raw = BASE64
        .decode(s.as_bytes())
        .context("macaroon parsing failed")?;
    let mac = Macaroon::deserialize(&raw)
        .map_err(|err| anyhow!("macaroon deserialization failed: {:?}", err))?;
    let mut editor_id: Option<String> = None;
    for caveat in mac.first_party_caveats() {
        if let Caveat::FirstParty(fp) = caveat {
            let predicate_str = String::from_utf8(fp.predicate().as_ref().to_vec())?;
            if predicate_str.starts_with("editor_id = ") {
                editor_id = Some(
                    predicate_str
                        .get(12..)
                        .context("parsing macaroon")?
                        .to_string(),
                );
                break;
            }
        }
    }
    let editor_id = match editor_id {
        Some(id) => id,
        None => return Err(anyhow!("expected an editor_id caveat in macaroon token")),
    };
    Ok(editor_id)
}

pub fn path_or_stdin(raw: Option<PathBuf>) -> Option<PathBuf> {
    // treat "-" as "use stdin"
    match raw {
        Some(s) if s.to_string_lossy() == "-" => None,
        _ => raw,
    }
}
