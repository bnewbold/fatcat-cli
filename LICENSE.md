
The swagger/openapi API schema file (eg, ./fatcat-openapi3.yml) is released
under the permissive MIT license. The schema files are:

    Copyright, 2018-2021 Bryan Newbold <bnewbold@robocracy.org)

The rust 'fatcat-openapi' API library (under the ./fatcat-openapi/
directory), which is auto-generated from the swagger/openapi schema
specification, is released to the public domain.

The rust 'fatcat-cli' tool, tests, scripts, and associated programs (under
the ./fatcat-cli/ directory), unless otherwise specified are released under
the GNU General Public License, version 3. A copy of this license should
be included in this repository. Unless otherwise indicated this code is:

    Copyright, 2020-2021 Bryan Newbold <bnewbold@robocracy.org)

A copy of the GPLv3 license can also be found at:

    https://www.gnu.org/licenses/gpl-3.0.en.html

A copy of the MIT license can also be found at:

    https://opensource.org/licenses/MIT

For questions and clarification, contact Bryan Newbold <bnewbold@robocracy.org>
