
possible features:
- bot management
- save-paper-now
- biblio transforms: CLS-JSON, bibtex, 
- fuzzy matching
- citation graph
- with extra auth, fetch derived files (eg, GROBID TEI-XML)
- merge (redirect) entities
- revert editgroups
- retry downloads from log

