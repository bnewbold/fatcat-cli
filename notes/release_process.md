
Quick reminder of how to do a versioned release:

- do any safe formatting, linting, clippy updates
- update documentation of any changes (manpage, README, etc)
- re-generate manpage, shell completions
- push to remote and verify that tests are passing in CI
- create a local commit that updates version number in all metadata/documentation
    => Cargo.toml and Cargo.lock (run a build/check)
    => CHANGELOG (include current date)
- build debian package and linux binary (`--release`)
- tag version number commit and push commit and tag to remote
- on a macOS machine, pull tagged version and run a `--release` build, then create homebrew tarball
- upload `.deb`, linux binary, and homebrew tarball to binary distribution archive.org item
- update homebrew repository for homebrew version
- create a new commit which increments the version metadata and adds `-dev`; push to remote
    => Cargo.toml and Cargo.lock (run a build/check)
