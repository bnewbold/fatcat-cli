
# CHANGELOG

## [0.1.8] - 2022-10-20

This is a dependency-only update, no change in behavior or features.

### Changed

- update `macaroon` crate to v0.3.0
- update `sha1` crate to `sha1_smol`

## [0.1.7] - 2022-09-07

### Added

- more `release` entities fields can be updated

### Changed

- update dependencies
- increase MSRV to 1.59
- use newer openapi code generator (v6.0.1)

## [0.1.6] - 2021-11-17

### Added

- support for mutation of a small handful of additional entity fields
- optional `--description` argument for batch edit operations

### Changed

- updated to API version v0.5.0, for `content_scope` fields

## [0.1.5] - 2021-10-14

### Changed

- updated to API version v0.4.0, notably including 'issn' container lookup,
  'hdl' release identifier, and editor lookups by username

## [0.1.4] - 2021-08-10

### Added

- basic search for reference index (refs, refs-in, refs-out)
- `--entity-json` mode for fulltext (scholar) search, returning biblio
  (primary) release entity

## [0.1.3] - 2021-05-25

### Fixed

- wayback URLs actually rewritten correctly for downloads

### Added

- search of `scholar` (fulltext) index

### Changed

- file and container search queries are much improved
- cargo clippy clean

## [0.1.2] - 2021-04-09

Quick release to fix searching with new Elasticsearch 7.10 endpoint.

### Fixed

- search code tweaked for ES 7.x: count all hits, and parse total hits field
- batch downloading from file (not stdin) was running single-threaded
- concurrency bug resulted in batch download not terminating

### Changed

- prefer archive.org URLs when downloading

## [0.1.1] - 2021-02-15

Small release, mostly documentation updates.

### Changed

- editgroup ids can be specified with `editgroup_`

### Added

- `status` command shows version of fatcat API schema that client was compiled from

## [0.1.0] - 2021-02-10

First tagged release, with core features:

- basic debian (apt) and macOS (homebrew) packaging
- editing and editgroup management commands
- search commands
- batch commands
- openapi3 schema and code-generated library for fatcat API v0.3.3
- limited "update" entity field mutations
