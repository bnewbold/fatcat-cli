
<div align="center">
<!-- TODO: move to an archive.org item... or just include file in this repo? -->
<!-- attribution is at bottom of this page -->
<a href="https://www.flickr.com/photos/threecheersformcr_xo/5340309206">
<img src="http://static.bnewbold.net/tmp/keyboard-cat-flickr-threecheersformcr_xo.jpg">
</a>
</div>

`fatcat-cli`: command-line utility for fatcat catalog API
=========================================================

[![build status](https://gitlab.com/bnewbold/fatcat-cli/badges/master/pipeline.svg)](https://gitlab.com/bnewbold/fatcat-cli/commits/master)

This tool should not be confused with the `fatcat` utility packaged for
Debian/Ubuntu, which is used to debug FAT filesystem images. If you aren't
using that tool, and do use this tool a bunch, you might consider adding an
`alias` or symlink so the "`fatcat`" command runs "`fatcat-cli`".

**DISCLAIMER:** this tool is still under development. The interface (arguments,
flags) and default behaviors are not yet stable.

## Install

Debian/Ubuntu Linux users can install bare `.deb` packages. Download the most
recent from <http://archive.org/download/ia-fatcat-cli-bin>, then install with:

    sudo apt install ./fatcat-cli-*.deb

MacOS Homebrew users with Intel CPUs, can install using a "tap":

    brew install bnewbold/fatcat/fatcat-cli

<!--
Users on all other platforms can use the Rust `cargo` tool to build and install
the utility for their user. Unfortunately this will not include the manual
("man page") or shell completions:

    cargo install fatcat-cli
-->

## Quickstart

Query the catalog:

    fatcat-cli search releases "metadata author:phillips"

Fetch metadata for a specific work:

    fatcat-cli get doi:10.1002/spe.659

Download 100 papers from a specific journal, as PDF:

    fatcat-cli search releases journal:"first monday" --entity-json --expand files | fatcat-cli batch download --limit 100

### Authentication

To propose changes to the catalog, you need a <https://fatcat.wiki> account.
You can create one quickly by logging in with an existing Internet Archive,
ORCiD, Wikipedia, or Gitlab account. Create a new API token from the [account
page](https://fatcat.wiki/auth/account), and set this token (a long sequence of
characters) as an environment variable in your shell:

    export FATCAT_API_AUTH_TOKEN=...

You could put this in a secrets/password manager so you don't lose it. Or,
depending on your setup, in a`~/.profile`. The tool does not (yet)
automatically load "dotenv" (`./.env`) files, but you might be using a
project-management tool which does so already.

You can check the status of your authentication and connection to the server
with:

    fatcat-cli status

### Editing

Every change to the catalog (an "edit") is made as part of an "editgroup". In
some cases the CLI tool with create or guess what the current editgroup you are
working on is, but you can also create them explicitly and pass the editgroup
identifier on every subsequent edit. It is best to combine small groups of
related changes into the same editgroup (so they can be reviewed together), but
to split up larger batches into editgroups of 50-100 changes at a time.

Individual entities can be edited from the convenience of your text editor, in
either JSON or TOML format:

    fatcat-cli get release_hsmo6p4smrganpb3fndaj2lon4 --json > release_hsmo6p4smrganpb3fndaj2lon4.json

    # whatever editor you prefer
    emacs release_hsmo6p4smrganpb3fndaj2lon4

    fatcat-cli update release_hsmo6p4smrganpb3fndaj2lon4 < release_hsmo6p4smrganpb3fndaj2lon4.
json

Or, with a single command:

    fatcat-cli edit release_hsmo6p4smrganpb3fndaj2lon4 --toml

To check in on the status of recent editgroups, or to "submit" them for review:

    fatcat-cli editgroups list
    fatcat-cli editgroups submit editgroup_...


## Development

You need a Rust toolchain installed, and the `libsodium-dev` system package.
There are Makefile helpers, run `make help` to print a list of commands.

The Minimum Supported Rust Version (MSRV) is 1.59 (released in 2022).


## Thanks!

The "keyboard cat" photo at the top of this README is by Cassandra Leigh Gotto ([threecheersformcr_xo](https://www.flickr.com/photos/threecheersformcr_xo/5340309206/)) and shared under the [CC-BY-NC](https://creativecommons.org/licenses/by-nc/2.0/) license.
