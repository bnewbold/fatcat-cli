//! Main library entry point for fatcat_openapi implementation.

#![allow(unused_imports)]

use async_trait::async_trait;
use futures::{future, Stream, StreamExt, TryFutureExt, TryStreamExt};
use hyper::server::conn::Http;
use hyper::service::Service;
use log::info;
use std::future::Future;
use std::marker::PhantomData;
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use swagger::auth::MakeAllowAllAuthenticator;
use swagger::EmptyContext;
use swagger::{Has, XSpanIdString};
use tokio::net::TcpListener;

#[cfg(not(any(target_os = "macos", target_os = "windows", target_os = "ios")))]
use openssl::ssl::{Ssl, SslAcceptor, SslAcceptorBuilder, SslFiletype, SslMethod};

use fatcat_openapi::models;

/// Builds an SSL implementation for Simple HTTPS from some hard-coded file names
pub async fn create(addr: &str, https: bool) {
    let addr = addr.parse().expect("Failed to parse bind address");

    let server = Server::new();

    let service = MakeService::new(server);

    let service = MakeAllowAllAuthenticator::new(service, "cosmo");

    let mut service =
        fatcat_openapi::server::context::MakeAddContext::<_, EmptyContext>::new(service);

    if https {
        #[cfg(any(target_os = "macos", target_os = "windows", target_os = "ios"))]
        {
            unimplemented!("SSL is not implemented for the examples on MacOS, Windows or iOS");
        }

        #[cfg(not(any(target_os = "macos", target_os = "windows", target_os = "ios")))]
        {
            let mut ssl = SslAcceptor::mozilla_intermediate_v5(SslMethod::tls())
                .expect("Failed to create SSL Acceptor");

            // Server authentication
            ssl.set_private_key_file("examples/server-key.pem", SslFiletype::PEM)
                .expect("Failed to set private key");
            ssl.set_certificate_chain_file("examples/server-chain.pem")
                .expect("Failed to set certificate chain");
            ssl.check_private_key()
                .expect("Failed to check private key");

            let tls_acceptor = ssl.build();
            let tcp_listener = TcpListener::bind(&addr).await.unwrap();

            loop {
                if let Ok((tcp, _)) = tcp_listener.accept().await {
                    let ssl = Ssl::new(tls_acceptor.context()).unwrap();
                    let addr = tcp.peer_addr().expect("Unable to get remote address");
                    let service = service.call(addr);

                    tokio::spawn(async move {
                        let tls = tokio_openssl::SslStream::new(ssl, tcp).map_err(|_| ())?;
                        let service = service.await.map_err(|_| ())?;

                        Http::new()
                            .serve_connection(tls, service)
                            .await
                            .map_err(|_| ())
                    });
                }
            }
        }
    } else {
        // Using HTTP
        hyper::server::Server::bind(&addr)
            .serve(service)
            .await
            .unwrap()
    }
}

#[derive(Copy, Clone)]
pub struct Server<C> {
    marker: PhantomData<C>,
}

impl<C> Server<C> {
    pub fn new() -> Self {
        Server {
            marker: PhantomData,
        }
    }
}

use fatcat_openapi::server::MakeService;
use fatcat_openapi::{
    AcceptEditgroupResponse, Api, AuthCheckResponse, AuthOidcResponse, CreateAuthTokenResponse,
    CreateContainerAutoBatchResponse, CreateContainerResponse, CreateCreatorAutoBatchResponse,
    CreateCreatorResponse, CreateEditgroupAnnotationResponse, CreateEditgroupResponse,
    CreateFileAutoBatchResponse, CreateFileResponse, CreateFilesetAutoBatchResponse,
    CreateFilesetResponse, CreateReleaseAutoBatchResponse, CreateReleaseResponse,
    CreateWebcaptureAutoBatchResponse, CreateWebcaptureResponse, CreateWorkAutoBatchResponse,
    CreateWorkResponse, DeleteContainerEditResponse, DeleteContainerResponse,
    DeleteCreatorEditResponse, DeleteCreatorResponse, DeleteFileEditResponse, DeleteFileResponse,
    DeleteFilesetEditResponse, DeleteFilesetResponse, DeleteReleaseEditResponse,
    DeleteReleaseResponse, DeleteWebcaptureEditResponse, DeleteWebcaptureResponse,
    DeleteWorkEditResponse, DeleteWorkResponse, GetChangelogEntryResponse, GetChangelogResponse,
    GetContainerEditResponse, GetContainerHistoryResponse, GetContainerRedirectsResponse,
    GetContainerResponse, GetContainerRevisionResponse, GetCreatorEditResponse,
    GetCreatorHistoryResponse, GetCreatorRedirectsResponse, GetCreatorReleasesResponse,
    GetCreatorResponse, GetCreatorRevisionResponse, GetEditgroupAnnotationsResponse,
    GetEditgroupResponse, GetEditgroupsReviewableResponse, GetEditorAnnotationsResponse,
    GetEditorEditgroupsResponse, GetEditorResponse, GetFileEditResponse, GetFileHistoryResponse,
    GetFileRedirectsResponse, GetFileResponse, GetFileRevisionResponse, GetFilesetEditResponse,
    GetFilesetHistoryResponse, GetFilesetRedirectsResponse, GetFilesetResponse,
    GetFilesetRevisionResponse, GetReleaseEditResponse, GetReleaseFilesResponse,
    GetReleaseFilesetsResponse, GetReleaseHistoryResponse, GetReleaseRedirectsResponse,
    GetReleaseResponse, GetReleaseRevisionResponse, GetReleaseWebcapturesResponse,
    GetWebcaptureEditResponse, GetWebcaptureHistoryResponse, GetWebcaptureRedirectsResponse,
    GetWebcaptureResponse, GetWebcaptureRevisionResponse, GetWorkEditResponse,
    GetWorkHistoryResponse, GetWorkRedirectsResponse, GetWorkReleasesResponse, GetWorkResponse,
    GetWorkRevisionResponse, LookupContainerResponse, LookupCreatorResponse, LookupEditorResponse,
    LookupFileResponse, LookupReleaseResponse, UpdateContainerResponse, UpdateCreatorResponse,
    UpdateEditgroupResponse, UpdateEditorResponse, UpdateFileResponse, UpdateFilesetResponse,
    UpdateReleaseResponse, UpdateWebcaptureResponse, UpdateWorkResponse,
};
use std::error::Error;
use swagger::ApiError;

#[async_trait]
impl<C> Api<C> for Server<C>
where
    C: Has<XSpanIdString> + Send + Sync,
{
    async fn accept_editgroup(
        &self,
        editgroup_id: String,
        context: &C,
    ) -> Result<AcceptEditgroupResponse, ApiError> {
        let context = context.clone();
        info!(
            "accept_editgroup(\"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn auth_check(
        &self,
        role: Option<String>,
        context: &C,
    ) -> Result<AuthCheckResponse, ApiError> {
        let context = context.clone();
        info!(
            "auth_check({:?}) - X-Span-ID: {:?}",
            role,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn auth_oidc(
        &self,
        auth_oidc: models::AuthOidc,
        context: &C,
    ) -> Result<AuthOidcResponse, ApiError> {
        let context = context.clone();
        info!(
            "auth_oidc({:?}) - X-Span-ID: {:?}",
            auth_oidc,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_auth_token(
        &self,
        editor_id: String,
        duration_seconds: Option<i32>,
        context: &C,
    ) -> Result<CreateAuthTokenResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_auth_token(\"{}\", {:?}) - X-Span-ID: {:?}",
            editor_id,
            duration_seconds,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_container(
        &self,
        editgroup_id: String,
        container_entity: models::ContainerEntity,
        context: &C,
    ) -> Result<CreateContainerResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_container(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            container_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_container_auto_batch(
        &self,
        container_auto_batch: models::ContainerAutoBatch,
        context: &C,
    ) -> Result<CreateContainerAutoBatchResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_container_auto_batch({:?}) - X-Span-ID: {:?}",
            container_auto_batch,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_creator(
        &self,
        editgroup_id: String,
        creator_entity: models::CreatorEntity,
        context: &C,
    ) -> Result<CreateCreatorResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_creator(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            creator_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_creator_auto_batch(
        &self,
        creator_auto_batch: models::CreatorAutoBatch,
        context: &C,
    ) -> Result<CreateCreatorAutoBatchResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_creator_auto_batch({:?}) - X-Span-ID: {:?}",
            creator_auto_batch,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_editgroup(
        &self,
        editgroup: models::Editgroup,
        context: &C,
    ) -> Result<CreateEditgroupResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_editgroup({:?}) - X-Span-ID: {:?}",
            editgroup,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_editgroup_annotation(
        &self,
        editgroup_id: String,
        editgroup_annotation: models::EditgroupAnnotation,
        context: &C,
    ) -> Result<CreateEditgroupAnnotationResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_editgroup_annotation(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            editgroup_annotation,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_file(
        &self,
        editgroup_id: String,
        file_entity: models::FileEntity,
        context: &C,
    ) -> Result<CreateFileResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_file(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            file_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_file_auto_batch(
        &self,
        file_auto_batch: models::FileAutoBatch,
        context: &C,
    ) -> Result<CreateFileAutoBatchResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_file_auto_batch({:?}) - X-Span-ID: {:?}",
            file_auto_batch,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_fileset(
        &self,
        editgroup_id: String,
        fileset_entity: models::FilesetEntity,
        context: &C,
    ) -> Result<CreateFilesetResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_fileset(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            fileset_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_fileset_auto_batch(
        &self,
        fileset_auto_batch: models::FilesetAutoBatch,
        context: &C,
    ) -> Result<CreateFilesetAutoBatchResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_fileset_auto_batch({:?}) - X-Span-ID: {:?}",
            fileset_auto_batch,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_release(
        &self,
        editgroup_id: String,
        release_entity: models::ReleaseEntity,
        context: &C,
    ) -> Result<CreateReleaseResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_release(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            release_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_release_auto_batch(
        &self,
        release_auto_batch: models::ReleaseAutoBatch,
        context: &C,
    ) -> Result<CreateReleaseAutoBatchResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_release_auto_batch({:?}) - X-Span-ID: {:?}",
            release_auto_batch,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_webcapture(
        &self,
        editgroup_id: String,
        webcapture_entity: models::WebcaptureEntity,
        context: &C,
    ) -> Result<CreateWebcaptureResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_webcapture(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            webcapture_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_webcapture_auto_batch(
        &self,
        webcapture_auto_batch: models::WebcaptureAutoBatch,
        context: &C,
    ) -> Result<CreateWebcaptureAutoBatchResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_webcapture_auto_batch({:?}) - X-Span-ID: {:?}",
            webcapture_auto_batch,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_work(
        &self,
        editgroup_id: String,
        work_entity: models::WorkEntity,
        context: &C,
    ) -> Result<CreateWorkResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_work(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            work_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn create_work_auto_batch(
        &self,
        work_auto_batch: models::WorkAutoBatch,
        context: &C,
    ) -> Result<CreateWorkAutoBatchResponse, ApiError> {
        let context = context.clone();
        info!(
            "create_work_auto_batch({:?}) - X-Span-ID: {:?}",
            work_auto_batch,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_container(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteContainerResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_container(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_container_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteContainerEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_container_edit(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_creator(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteCreatorResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_creator(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_creator_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteCreatorEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_creator_edit(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_file(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteFileResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_file(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_file_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteFileEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_file_edit(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_fileset(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteFilesetResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_fileset(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_fileset_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteFilesetEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_fileset_edit(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_release(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteReleaseResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_release(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_release_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteReleaseEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_release_edit(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteWebcaptureResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_webcapture(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_webcapture_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteWebcaptureEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_webcapture_edit(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_work(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteWorkResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_work(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn delete_work_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteWorkEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_work_edit(\"{}\", \"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_changelog(
        &self,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetChangelogResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_changelog({:?}) - X-Span-ID: {:?}",
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_changelog_entry(
        &self,
        index: i64,
        context: &C,
    ) -> Result<GetChangelogEntryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_changelog_entry({}) - X-Span-ID: {:?}",
            index,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_container(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetContainerResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_container(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            ident,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_container_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetContainerEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_container_edit(\"{}\") - X-Span-ID: {:?}",
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_container_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetContainerHistoryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_container_history(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_container_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetContainerRedirectsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_container_redirects(\"{}\") - X-Span-ID: {:?}",
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_container_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetContainerRevisionResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_container_revision(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            rev_id,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_creator(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetCreatorResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_creator(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            ident,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_creator_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetCreatorEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_creator_edit(\"{}\") - X-Span-ID: {:?}",
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_creator_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetCreatorHistoryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_creator_history(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_creator_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetCreatorRedirectsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_creator_redirects(\"{}\") - X-Span-ID: {:?}",
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_creator_releases(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetCreatorReleasesResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_creator_releases(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_creator_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetCreatorRevisionResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_creator_revision(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            rev_id,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_editgroup(
        &self,
        editgroup_id: String,
        context: &C,
    ) -> Result<GetEditgroupResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_editgroup(\"{}\") - X-Span-ID: {:?}",
            editgroup_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_editgroup_annotations(
        &self,
        editgroup_id: String,
        expand: Option<String>,
        context: &C,
    ) -> Result<GetEditgroupAnnotationsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_editgroup_annotations(\"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            expand,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_editgroups_reviewable(
        &self,
        expand: Option<String>,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
        context: &C,
    ) -> Result<GetEditgroupsReviewableResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_editgroups_reviewable({:?}, {:?}, {:?}, {:?}) - X-Span-ID: {:?}",
            expand,
            limit,
            before,
            since,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_editor(
        &self,
        editor_id: String,
        context: &C,
    ) -> Result<GetEditorResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_editor(\"{}\") - X-Span-ID: {:?}",
            editor_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_editor_annotations(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
        context: &C,
    ) -> Result<GetEditorAnnotationsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_editor_annotations(\"{}\", {:?}, {:?}, {:?}) - X-Span-ID: {:?}",
            editor_id,
            limit,
            before,
            since,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_editor_editgroups(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
        context: &C,
    ) -> Result<GetEditorEditgroupsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_editor_editgroups(\"{}\", {:?}, {:?}, {:?}) - X-Span-ID: {:?}",
            editor_id,
            limit,
            before,
            since,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_file(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFileResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_file(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            ident,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_file_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetFileEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_file_edit(\"{}\") - X-Span-ID: {:?}",
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_file_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetFileHistoryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_file_history(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_file_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetFileRedirectsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_file_redirects(\"{}\") - X-Span-ID: {:?}",
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_file_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFileRevisionResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_file_revision(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            rev_id,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_fileset(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFilesetResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_fileset(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            ident,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_fileset_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetFilesetEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_fileset_edit(\"{}\") - X-Span-ID: {:?}",
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_fileset_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetFilesetHistoryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_fileset_history(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_fileset_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetFilesetRedirectsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_fileset_redirects(\"{}\") - X-Span-ID: {:?}",
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_fileset_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFilesetRevisionResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_fileset_revision(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            rev_id,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            ident,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetReleaseEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release_edit(\"{}\") - X-Span-ID: {:?}",
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release_files(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseFilesResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release_files(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release_filesets(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseFilesetsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release_filesets(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetReleaseHistoryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release_history(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetReleaseRedirectsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release_redirects(\"{}\") - X-Span-ID: {:?}",
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseRevisionResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release_revision(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            rev_id,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_release_webcaptures(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseWebcapturesResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_release_webcaptures(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_webcapture(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWebcaptureResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_webcapture(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            ident,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_webcapture_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetWebcaptureEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_webcapture_edit(\"{}\") - X-Span-ID: {:?}",
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_webcapture_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetWebcaptureHistoryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_webcapture_history(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_webcapture_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetWebcaptureRedirectsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_webcapture_redirects(\"{}\") - X-Span-ID: {:?}",
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_webcapture_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWebcaptureRevisionResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_webcapture_revision(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            rev_id,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_work(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWorkResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_work(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            ident,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_work_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetWorkEditResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_work_edit(\"{}\") - X-Span-ID: {:?}",
            edit_id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_work_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetWorkHistoryResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_work_history(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            limit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_work_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetWorkRedirectsResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_work_redirects(\"{}\") - X-Span-ID: {:?}",
            ident,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_work_releases(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWorkReleasesResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_work_releases(\"{}\", {:?}) - X-Span-ID: {:?}",
            ident,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn get_work_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWorkRevisionResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_work_revision(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            rev_id,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn lookup_container(
        &self,
        issnl: Option<String>,
        issne: Option<String>,
        issnp: Option<String>,
        issn: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupContainerResponse, ApiError> {
        let context = context.clone();
        info!(
            "lookup_container({:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}) - X-Span-ID: {:?}",
            issnl,
            issne,
            issnp,
            issn,
            wikidata_qid,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn lookup_creator(
        &self,
        orcid: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupCreatorResponse, ApiError> {
        let context = context.clone();
        info!(
            "lookup_creator({:?}, {:?}, {:?}, {:?}) - X-Span-ID: {:?}",
            orcid,
            wikidata_qid,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn lookup_editor(
        &self,
        username: Option<String>,
        context: &C,
    ) -> Result<LookupEditorResponse, ApiError> {
        let context = context.clone();
        info!(
            "lookup_editor({:?}) - X-Span-ID: {:?}",
            username,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn lookup_file(
        &self,
        md5: Option<String>,
        sha1: Option<String>,
        sha256: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupFileResponse, ApiError> {
        let context = context.clone();
        info!(
            "lookup_file({:?}, {:?}, {:?}, {:?}, {:?}) - X-Span-ID: {:?}",
            md5,
            sha1,
            sha256,
            expand,
            hide,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn lookup_release(
        &self,
        doi: Option<String>,
        wikidata_qid: Option<String>,
        isbn13: Option<String>,
        pmid: Option<String>,
        pmcid: Option<String>,
        core: Option<String>,
        arxiv: Option<String>,
        jstor: Option<String>,
        ark: Option<String>,
        mag: Option<String>,
        doaj: Option<String>,
        dblp: Option<String>,
        oai: Option<String>,
        hdl: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupReleaseResponse, ApiError> {
        let context = context.clone();
        info!("lookup_release({:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}) - X-Span-ID: {:?}", doi, wikidata_qid, isbn13, pmid, pmcid, core, arxiv, jstor, ark, mag, doaj, dblp, oai, hdl, expand, hide, context.get().0.clone());
        Err(ApiError("Generic failure".into()))
    }

    async fn update_container(
        &self,
        editgroup_id: String,
        ident: String,
        container_entity: models::ContainerEntity,
        context: &C,
    ) -> Result<UpdateContainerResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_container(\"{}\", \"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            container_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_creator(
        &self,
        editgroup_id: String,
        ident: String,
        creator_entity: models::CreatorEntity,
        context: &C,
    ) -> Result<UpdateCreatorResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_creator(\"{}\", \"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            creator_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_editgroup(
        &self,
        editgroup_id: String,
        editgroup: models::Editgroup,
        submit: Option<bool>,
        context: &C,
    ) -> Result<UpdateEditgroupResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_editgroup(\"{}\", {:?}, {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            editgroup,
            submit,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_editor(
        &self,
        editor_id: String,
        editor: models::Editor,
        context: &C,
    ) -> Result<UpdateEditorResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_editor(\"{}\", {:?}) - X-Span-ID: {:?}",
            editor_id,
            editor,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_file(
        &self,
        editgroup_id: String,
        ident: String,
        file_entity: models::FileEntity,
        context: &C,
    ) -> Result<UpdateFileResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_file(\"{}\", \"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            file_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_fileset(
        &self,
        editgroup_id: String,
        ident: String,
        fileset_entity: models::FilesetEntity,
        context: &C,
    ) -> Result<UpdateFilesetResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_fileset(\"{}\", \"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            fileset_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_release(
        &self,
        editgroup_id: String,
        ident: String,
        release_entity: models::ReleaseEntity,
        context: &C,
    ) -> Result<UpdateReleaseResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_release(\"{}\", \"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            release_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
        webcapture_entity: models::WebcaptureEntity,
        context: &C,
    ) -> Result<UpdateWebcaptureResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_webcapture(\"{}\", \"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            webcapture_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }

    async fn update_work(
        &self,
        editgroup_id: String,
        ident: String,
        work_entity: models::WorkEntity,
        context: &C,
    ) -> Result<UpdateWorkResponse, ApiError> {
        let context = context.clone();
        info!(
            "update_work(\"{}\", \"{}\", {:?}) - X-Span-ID: {:?}",
            editgroup_id,
            ident,
            work_entity,
            context.get().0.clone()
        );
        Err(ApiError("Generic failure".into()))
    }
}
