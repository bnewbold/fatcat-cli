#![allow(
    missing_docs,
    trivial_casts,
    unused_variables,
    unused_mut,
    unused_imports,
    unused_extern_crates,
    non_camel_case_types
)]

use async_trait::async_trait;
use futures::Stream;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::task::{Context, Poll};
use swagger::{ApiError, ContextWrapper};

type ServiceError = Box<dyn Error + Send + Sync + 'static>;

pub const BASE_PATH: &'static str = "/v0";
pub const API_VERSION: &'static str = "0.5.0";

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum AcceptEditgroupResponse {
    /// Merged Successfully
    MergedSuccessfully(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Edit Conflict
    EditConflict(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum AuthCheckResponse {
    /// Success
    Success(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum AuthOidcResponse {
    /// Found
    Found(models::AuthOidcResult),
    /// Created
    Created(models::AuthOidcResult),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Conflict
    Conflict(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateAuthTokenResponse {
    /// Success
    Success(models::AuthTokenResult),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateContainerResponse {
    /// Created Entity
    CreatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateContainerAutoBatchResponse {
    /// Created Editgroup
    CreatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateCreatorResponse {
    /// Created Entity
    CreatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateCreatorAutoBatchResponse {
    /// Created Editgroup
    CreatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateEditgroupResponse {
    /// Successfully Created
    SuccessfullyCreated(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateEditgroupAnnotationResponse {
    /// Created
    Created(models::EditgroupAnnotation),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateFileResponse {
    /// Created Entity
    CreatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateFileAutoBatchResponse {
    /// Created Editgroup
    CreatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateFilesetResponse {
    /// Created Entity
    CreatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateFilesetAutoBatchResponse {
    /// Created Editgroup
    CreatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateReleaseResponse {
    /// Created Entity
    CreatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateReleaseAutoBatchResponse {
    /// Created Editgroup
    CreatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateWebcaptureResponse {
    /// Created Entity
    CreatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateWebcaptureAutoBatchResponse {
    /// Created Editgroup
    CreatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateWorkResponse {
    /// Created Entity
    CreatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum CreateWorkAutoBatchResponse {
    /// Created Editgroup
    CreatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteContainerResponse {
    /// Deleted Entity
    DeletedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteContainerEditResponse {
    /// Deleted Edit
    DeletedEdit(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteCreatorResponse {
    /// Deleted Entity
    DeletedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteCreatorEditResponse {
    /// Deleted Edit
    DeletedEdit(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteFileResponse {
    /// Deleted Entity
    DeletedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteFileEditResponse {
    /// Deleted Edit
    DeletedEdit(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteFilesetResponse {
    /// Deleted Entity
    DeletedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteFilesetEditResponse {
    /// Deleted Edit
    DeletedEdit(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteReleaseResponse {
    /// Deleted Entity
    DeletedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteReleaseEditResponse {
    /// Deleted Edit
    DeletedEdit(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteWebcaptureResponse {
    /// Deleted Entity
    DeletedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteWebcaptureEditResponse {
    /// Deleted Edit
    DeletedEdit(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteWorkResponse {
    /// Deleted Entity
    DeletedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum DeleteWorkEditResponse {
    /// Deleted Edit
    DeletedEdit(models::Success),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetChangelogResponse {
    /// Success
    Success(Vec<models::ChangelogEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetChangelogEntryResponse {
    /// Found Changelog Entry
    FoundChangelogEntry(models::ChangelogEntry),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetContainerResponse {
    /// Found Entity
    FoundEntity(models::ContainerEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetContainerEditResponse {
    /// Found Edit
    FoundEdit(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetContainerHistoryResponse {
    /// Found Entity History
    FoundEntityHistory(Vec<models::EntityHistoryEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetContainerRedirectsResponse {
    /// Found Entity Redirects
    FoundEntityRedirects(Vec<String>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetContainerRevisionResponse {
    /// Found Entity Revision
    FoundEntityRevision(models::ContainerEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetCreatorResponse {
    /// Found Entity
    FoundEntity(models::CreatorEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetCreatorEditResponse {
    /// Found Edit
    FoundEdit(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetCreatorHistoryResponse {
    /// Found Entity History
    FoundEntityHistory(Vec<models::EntityHistoryEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetCreatorRedirectsResponse {
    /// Found Entity Redirects
    FoundEntityRedirects(Vec<String>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetCreatorReleasesResponse {
    /// Found
    Found(Vec<models::ReleaseEntity>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetCreatorRevisionResponse {
    /// Found Entity Revision
    FoundEntityRevision(models::CreatorEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetEditgroupResponse {
    /// Found
    Found(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetEditgroupAnnotationsResponse {
    /// Success
    Success(Vec<models::EditgroupAnnotation>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetEditgroupsReviewableResponse {
    /// Found
    Found(Vec<models::Editgroup>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetEditorResponse {
    /// Found
    Found(models::Editor),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetEditorAnnotationsResponse {
    /// Success
    Success(Vec<models::EditgroupAnnotation>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetEditorEditgroupsResponse {
    /// Found
    Found(Vec<models::Editgroup>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFileResponse {
    /// Found Entity
    FoundEntity(models::FileEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFileEditResponse {
    /// Found Edit
    FoundEdit(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFileHistoryResponse {
    /// Found Entity History
    FoundEntityHistory(Vec<models::EntityHistoryEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFileRedirectsResponse {
    /// Found Entity Redirects
    FoundEntityRedirects(Vec<String>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFileRevisionResponse {
    /// Found Entity Revision
    FoundEntityRevision(models::FileEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFilesetResponse {
    /// Found Entity
    FoundEntity(models::FilesetEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFilesetEditResponse {
    /// Found Edit
    FoundEdit(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFilesetHistoryResponse {
    /// Found Entity History
    FoundEntityHistory(Vec<models::EntityHistoryEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFilesetRedirectsResponse {
    /// Found Entity Redirects
    FoundEntityRedirects(Vec<String>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetFilesetRevisionResponse {
    /// Found Entity Revision
    FoundEntityRevision(models::FilesetEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseResponse {
    /// Found Entity
    FoundEntity(models::ReleaseEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseEditResponse {
    /// Found Edit
    FoundEdit(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseFilesResponse {
    /// Found
    Found(Vec<models::FileEntity>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseFilesetsResponse {
    /// Found
    Found(Vec<models::FilesetEntity>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseHistoryResponse {
    /// Found Entity History
    FoundEntityHistory(Vec<models::EntityHistoryEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseRedirectsResponse {
    /// Found Entity Redirects
    FoundEntityRedirects(Vec<String>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseRevisionResponse {
    /// Found Entity Revision
    FoundEntityRevision(models::ReleaseEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetReleaseWebcapturesResponse {
    /// Found
    Found(Vec<models::WebcaptureEntity>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWebcaptureResponse {
    /// Found Entity
    FoundEntity(models::WebcaptureEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWebcaptureEditResponse {
    /// Found Edit
    FoundEdit(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWebcaptureHistoryResponse {
    /// Found Entity History
    FoundEntityHistory(Vec<models::EntityHistoryEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWebcaptureRedirectsResponse {
    /// Found Entity Redirects
    FoundEntityRedirects(Vec<String>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWebcaptureRevisionResponse {
    /// Found Entity Revision
    FoundEntityRevision(models::WebcaptureEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWorkResponse {
    /// Found Entity
    FoundEntity(models::WorkEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWorkEditResponse {
    /// Found Edit
    FoundEdit(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWorkHistoryResponse {
    /// Found Entity History
    FoundEntityHistory(Vec<models::EntityHistoryEntry>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWorkRedirectsResponse {
    /// Found Entity Redirects
    FoundEntityRedirects(Vec<String>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWorkReleasesResponse {
    /// Found
    Found(Vec<models::ReleaseEntity>),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetWorkRevisionResponse {
    /// Found Entity Revision
    FoundEntityRevision(models::WorkEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum LookupContainerResponse {
    /// Found Entity
    FoundEntity(models::ContainerEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum LookupCreatorResponse {
    /// Found Entity
    FoundEntity(models::CreatorEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum LookupEditorResponse {
    /// Found
    Found(models::Editor),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum LookupFileResponse {
    /// Found Entity
    FoundEntity(models::FileEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum LookupReleaseResponse {
    /// Found Entity
    FoundEntity(models::ReleaseEntity),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateContainerResponse {
    /// Updated Entity
    UpdatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateCreatorResponse {
    /// Updated Entity
    UpdatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateEditgroupResponse {
    /// Updated Editgroup
    UpdatedEditgroup(models::Editgroup),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateEditorResponse {
    /// Updated Editor
    UpdatedEditor(models::Editor),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateFileResponse {
    /// Updated Entity
    UpdatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateFilesetResponse {
    /// Updated Entity
    UpdatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateReleaseResponse {
    /// Updated Entity
    UpdatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateWebcaptureResponse {
    /// Updated Entity
    UpdatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum UpdateWorkResponse {
    /// Updated Entity
    UpdatedEntity(models::EntityEdit),
    /// Bad Request
    BadRequest(models::ErrorResponse),
    /// Not Authorized
    NotAuthorized {
        body: models::ErrorResponse,
        www_authenticate: Option<String>,
    },
    /// Forbidden
    Forbidden(models::ErrorResponse),
    /// Not Found
    NotFound(models::ErrorResponse),
    /// Generic Error
    GenericError(models::ErrorResponse),
}

/// API
#[async_trait]
pub trait Api<C: Send + Sync> {
    fn poll_ready(
        &self,
        _cx: &mut Context,
    ) -> Poll<Result<(), Box<dyn Error + Send + Sync + 'static>>> {
        Poll::Ready(Ok(()))
    }

    async fn accept_editgroup(
        &self,
        editgroup_id: String,
        context: &C,
    ) -> Result<AcceptEditgroupResponse, ApiError>;

    async fn auth_check(
        &self,
        role: Option<String>,
        context: &C,
    ) -> Result<AuthCheckResponse, ApiError>;

    async fn auth_oidc(
        &self,
        auth_oidc: models::AuthOidc,
        context: &C,
    ) -> Result<AuthOidcResponse, ApiError>;

    async fn create_auth_token(
        &self,
        editor_id: String,
        duration_seconds: Option<i32>,
        context: &C,
    ) -> Result<CreateAuthTokenResponse, ApiError>;

    async fn create_container(
        &self,
        editgroup_id: String,
        container_entity: models::ContainerEntity,
        context: &C,
    ) -> Result<CreateContainerResponse, ApiError>;

    async fn create_container_auto_batch(
        &self,
        container_auto_batch: models::ContainerAutoBatch,
        context: &C,
    ) -> Result<CreateContainerAutoBatchResponse, ApiError>;

    async fn create_creator(
        &self,
        editgroup_id: String,
        creator_entity: models::CreatorEntity,
        context: &C,
    ) -> Result<CreateCreatorResponse, ApiError>;

    async fn create_creator_auto_batch(
        &self,
        creator_auto_batch: models::CreatorAutoBatch,
        context: &C,
    ) -> Result<CreateCreatorAutoBatchResponse, ApiError>;

    async fn create_editgroup(
        &self,
        editgroup: models::Editgroup,
        context: &C,
    ) -> Result<CreateEditgroupResponse, ApiError>;

    async fn create_editgroup_annotation(
        &self,
        editgroup_id: String,
        editgroup_annotation: models::EditgroupAnnotation,
        context: &C,
    ) -> Result<CreateEditgroupAnnotationResponse, ApiError>;

    async fn create_file(
        &self,
        editgroup_id: String,
        file_entity: models::FileEntity,
        context: &C,
    ) -> Result<CreateFileResponse, ApiError>;

    async fn create_file_auto_batch(
        &self,
        file_auto_batch: models::FileAutoBatch,
        context: &C,
    ) -> Result<CreateFileAutoBatchResponse, ApiError>;

    async fn create_fileset(
        &self,
        editgroup_id: String,
        fileset_entity: models::FilesetEntity,
        context: &C,
    ) -> Result<CreateFilesetResponse, ApiError>;

    async fn create_fileset_auto_batch(
        &self,
        fileset_auto_batch: models::FilesetAutoBatch,
        context: &C,
    ) -> Result<CreateFilesetAutoBatchResponse, ApiError>;

    async fn create_release(
        &self,
        editgroup_id: String,
        release_entity: models::ReleaseEntity,
        context: &C,
    ) -> Result<CreateReleaseResponse, ApiError>;

    async fn create_release_auto_batch(
        &self,
        release_auto_batch: models::ReleaseAutoBatch,
        context: &C,
    ) -> Result<CreateReleaseAutoBatchResponse, ApiError>;

    async fn create_webcapture(
        &self,
        editgroup_id: String,
        webcapture_entity: models::WebcaptureEntity,
        context: &C,
    ) -> Result<CreateWebcaptureResponse, ApiError>;

    async fn create_webcapture_auto_batch(
        &self,
        webcapture_auto_batch: models::WebcaptureAutoBatch,
        context: &C,
    ) -> Result<CreateWebcaptureAutoBatchResponse, ApiError>;

    async fn create_work(
        &self,
        editgroup_id: String,
        work_entity: models::WorkEntity,
        context: &C,
    ) -> Result<CreateWorkResponse, ApiError>;

    async fn create_work_auto_batch(
        &self,
        work_auto_batch: models::WorkAutoBatch,
        context: &C,
    ) -> Result<CreateWorkAutoBatchResponse, ApiError>;

    async fn delete_container(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteContainerResponse, ApiError>;

    async fn delete_container_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteContainerEditResponse, ApiError>;

    async fn delete_creator(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteCreatorResponse, ApiError>;

    async fn delete_creator_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteCreatorEditResponse, ApiError>;

    async fn delete_file(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteFileResponse, ApiError>;

    async fn delete_file_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteFileEditResponse, ApiError>;

    async fn delete_fileset(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteFilesetResponse, ApiError>;

    async fn delete_fileset_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteFilesetEditResponse, ApiError>;

    async fn delete_release(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteReleaseResponse, ApiError>;

    async fn delete_release_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteReleaseEditResponse, ApiError>;

    async fn delete_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteWebcaptureResponse, ApiError>;

    async fn delete_webcapture_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteWebcaptureEditResponse, ApiError>;

    async fn delete_work(
        &self,
        editgroup_id: String,
        ident: String,
        context: &C,
    ) -> Result<DeleteWorkResponse, ApiError>;

    async fn delete_work_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
        context: &C,
    ) -> Result<DeleteWorkEditResponse, ApiError>;

    async fn get_changelog(
        &self,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetChangelogResponse, ApiError>;

    async fn get_changelog_entry(
        &self,
        index: i64,
        context: &C,
    ) -> Result<GetChangelogEntryResponse, ApiError>;

    async fn get_container(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetContainerResponse, ApiError>;

    async fn get_container_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetContainerEditResponse, ApiError>;

    async fn get_container_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetContainerHistoryResponse, ApiError>;

    async fn get_container_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetContainerRedirectsResponse, ApiError>;

    async fn get_container_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetContainerRevisionResponse, ApiError>;

    async fn get_creator(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetCreatorResponse, ApiError>;

    async fn get_creator_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetCreatorEditResponse, ApiError>;

    async fn get_creator_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetCreatorHistoryResponse, ApiError>;

    async fn get_creator_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetCreatorRedirectsResponse, ApiError>;

    async fn get_creator_releases(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetCreatorReleasesResponse, ApiError>;

    async fn get_creator_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetCreatorRevisionResponse, ApiError>;

    async fn get_editgroup(
        &self,
        editgroup_id: String,
        context: &C,
    ) -> Result<GetEditgroupResponse, ApiError>;

    async fn get_editgroup_annotations(
        &self,
        editgroup_id: String,
        expand: Option<String>,
        context: &C,
    ) -> Result<GetEditgroupAnnotationsResponse, ApiError>;

    async fn get_editgroups_reviewable(
        &self,
        expand: Option<String>,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
        context: &C,
    ) -> Result<GetEditgroupsReviewableResponse, ApiError>;

    async fn get_editor(
        &self,
        editor_id: String,
        context: &C,
    ) -> Result<GetEditorResponse, ApiError>;

    async fn get_editor_annotations(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
        context: &C,
    ) -> Result<GetEditorAnnotationsResponse, ApiError>;

    async fn get_editor_editgroups(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
        context: &C,
    ) -> Result<GetEditorEditgroupsResponse, ApiError>;

    async fn get_file(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFileResponse, ApiError>;

    async fn get_file_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetFileEditResponse, ApiError>;

    async fn get_file_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetFileHistoryResponse, ApiError>;

    async fn get_file_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetFileRedirectsResponse, ApiError>;

    async fn get_file_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFileRevisionResponse, ApiError>;

    async fn get_fileset(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFilesetResponse, ApiError>;

    async fn get_fileset_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetFilesetEditResponse, ApiError>;

    async fn get_fileset_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetFilesetHistoryResponse, ApiError>;

    async fn get_fileset_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetFilesetRedirectsResponse, ApiError>;

    async fn get_fileset_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetFilesetRevisionResponse, ApiError>;

    async fn get_release(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseResponse, ApiError>;

    async fn get_release_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetReleaseEditResponse, ApiError>;

    async fn get_release_files(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseFilesResponse, ApiError>;

    async fn get_release_filesets(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseFilesetsResponse, ApiError>;

    async fn get_release_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetReleaseHistoryResponse, ApiError>;

    async fn get_release_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetReleaseRedirectsResponse, ApiError>;

    async fn get_release_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseRevisionResponse, ApiError>;

    async fn get_release_webcaptures(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetReleaseWebcapturesResponse, ApiError>;

    async fn get_webcapture(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWebcaptureResponse, ApiError>;

    async fn get_webcapture_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetWebcaptureEditResponse, ApiError>;

    async fn get_webcapture_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetWebcaptureHistoryResponse, ApiError>;

    async fn get_webcapture_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetWebcaptureRedirectsResponse, ApiError>;

    async fn get_webcapture_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWebcaptureRevisionResponse, ApiError>;

    async fn get_work(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWorkResponse, ApiError>;

    async fn get_work_edit(
        &self,
        edit_id: String,
        context: &C,
    ) -> Result<GetWorkEditResponse, ApiError>;

    async fn get_work_history(
        &self,
        ident: String,
        limit: Option<i64>,
        context: &C,
    ) -> Result<GetWorkHistoryResponse, ApiError>;

    async fn get_work_redirects(
        &self,
        ident: String,
        context: &C,
    ) -> Result<GetWorkRedirectsResponse, ApiError>;

    async fn get_work_releases(
        &self,
        ident: String,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWorkReleasesResponse, ApiError>;

    async fn get_work_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<GetWorkRevisionResponse, ApiError>;

    async fn lookup_container(
        &self,
        issnl: Option<String>,
        issne: Option<String>,
        issnp: Option<String>,
        issn: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupContainerResponse, ApiError>;

    async fn lookup_creator(
        &self,
        orcid: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupCreatorResponse, ApiError>;

    async fn lookup_editor(
        &self,
        username: Option<String>,
        context: &C,
    ) -> Result<LookupEditorResponse, ApiError>;

    async fn lookup_file(
        &self,
        md5: Option<String>,
        sha1: Option<String>,
        sha256: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupFileResponse, ApiError>;

    async fn lookup_release(
        &self,
        doi: Option<String>,
        wikidata_qid: Option<String>,
        isbn13: Option<String>,
        pmid: Option<String>,
        pmcid: Option<String>,
        core: Option<String>,
        arxiv: Option<String>,
        jstor: Option<String>,
        ark: Option<String>,
        mag: Option<String>,
        doaj: Option<String>,
        dblp: Option<String>,
        oai: Option<String>,
        hdl: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
        context: &C,
    ) -> Result<LookupReleaseResponse, ApiError>;

    async fn update_container(
        &self,
        editgroup_id: String,
        ident: String,
        container_entity: models::ContainerEntity,
        context: &C,
    ) -> Result<UpdateContainerResponse, ApiError>;

    async fn update_creator(
        &self,
        editgroup_id: String,
        ident: String,
        creator_entity: models::CreatorEntity,
        context: &C,
    ) -> Result<UpdateCreatorResponse, ApiError>;

    async fn update_editgroup(
        &self,
        editgroup_id: String,
        editgroup: models::Editgroup,
        submit: Option<bool>,
        context: &C,
    ) -> Result<UpdateEditgroupResponse, ApiError>;

    async fn update_editor(
        &self,
        editor_id: String,
        editor: models::Editor,
        context: &C,
    ) -> Result<UpdateEditorResponse, ApiError>;

    async fn update_file(
        &self,
        editgroup_id: String,
        ident: String,
        file_entity: models::FileEntity,
        context: &C,
    ) -> Result<UpdateFileResponse, ApiError>;

    async fn update_fileset(
        &self,
        editgroup_id: String,
        ident: String,
        fileset_entity: models::FilesetEntity,
        context: &C,
    ) -> Result<UpdateFilesetResponse, ApiError>;

    async fn update_release(
        &self,
        editgroup_id: String,
        ident: String,
        release_entity: models::ReleaseEntity,
        context: &C,
    ) -> Result<UpdateReleaseResponse, ApiError>;

    async fn update_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
        webcapture_entity: models::WebcaptureEntity,
        context: &C,
    ) -> Result<UpdateWebcaptureResponse, ApiError>;

    async fn update_work(
        &self,
        editgroup_id: String,
        ident: String,
        work_entity: models::WorkEntity,
        context: &C,
    ) -> Result<UpdateWorkResponse, ApiError>;
}

/// API where `Context` isn't passed on every API call
#[async_trait]
pub trait ApiNoContext<C: Send + Sync> {
    fn poll_ready(
        &self,
        _cx: &mut Context,
    ) -> Poll<Result<(), Box<dyn Error + Send + Sync + 'static>>>;

    fn context(&self) -> &C;

    async fn accept_editgroup(
        &self,
        editgroup_id: String,
    ) -> Result<AcceptEditgroupResponse, ApiError>;

    async fn auth_check(&self, role: Option<String>) -> Result<AuthCheckResponse, ApiError>;

    async fn auth_oidc(&self, auth_oidc: models::AuthOidc) -> Result<AuthOidcResponse, ApiError>;

    async fn create_auth_token(
        &self,
        editor_id: String,
        duration_seconds: Option<i32>,
    ) -> Result<CreateAuthTokenResponse, ApiError>;

    async fn create_container(
        &self,
        editgroup_id: String,
        container_entity: models::ContainerEntity,
    ) -> Result<CreateContainerResponse, ApiError>;

    async fn create_container_auto_batch(
        &self,
        container_auto_batch: models::ContainerAutoBatch,
    ) -> Result<CreateContainerAutoBatchResponse, ApiError>;

    async fn create_creator(
        &self,
        editgroup_id: String,
        creator_entity: models::CreatorEntity,
    ) -> Result<CreateCreatorResponse, ApiError>;

    async fn create_creator_auto_batch(
        &self,
        creator_auto_batch: models::CreatorAutoBatch,
    ) -> Result<CreateCreatorAutoBatchResponse, ApiError>;

    async fn create_editgroup(
        &self,
        editgroup: models::Editgroup,
    ) -> Result<CreateEditgroupResponse, ApiError>;

    async fn create_editgroup_annotation(
        &self,
        editgroup_id: String,
        editgroup_annotation: models::EditgroupAnnotation,
    ) -> Result<CreateEditgroupAnnotationResponse, ApiError>;

    async fn create_file(
        &self,
        editgroup_id: String,
        file_entity: models::FileEntity,
    ) -> Result<CreateFileResponse, ApiError>;

    async fn create_file_auto_batch(
        &self,
        file_auto_batch: models::FileAutoBatch,
    ) -> Result<CreateFileAutoBatchResponse, ApiError>;

    async fn create_fileset(
        &self,
        editgroup_id: String,
        fileset_entity: models::FilesetEntity,
    ) -> Result<CreateFilesetResponse, ApiError>;

    async fn create_fileset_auto_batch(
        &self,
        fileset_auto_batch: models::FilesetAutoBatch,
    ) -> Result<CreateFilesetAutoBatchResponse, ApiError>;

    async fn create_release(
        &self,
        editgroup_id: String,
        release_entity: models::ReleaseEntity,
    ) -> Result<CreateReleaseResponse, ApiError>;

    async fn create_release_auto_batch(
        &self,
        release_auto_batch: models::ReleaseAutoBatch,
    ) -> Result<CreateReleaseAutoBatchResponse, ApiError>;

    async fn create_webcapture(
        &self,
        editgroup_id: String,
        webcapture_entity: models::WebcaptureEntity,
    ) -> Result<CreateWebcaptureResponse, ApiError>;

    async fn create_webcapture_auto_batch(
        &self,
        webcapture_auto_batch: models::WebcaptureAutoBatch,
    ) -> Result<CreateWebcaptureAutoBatchResponse, ApiError>;

    async fn create_work(
        &self,
        editgroup_id: String,
        work_entity: models::WorkEntity,
    ) -> Result<CreateWorkResponse, ApiError>;

    async fn create_work_auto_batch(
        &self,
        work_auto_batch: models::WorkAutoBatch,
    ) -> Result<CreateWorkAutoBatchResponse, ApiError>;

    async fn delete_container(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteContainerResponse, ApiError>;

    async fn delete_container_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteContainerEditResponse, ApiError>;

    async fn delete_creator(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteCreatorResponse, ApiError>;

    async fn delete_creator_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteCreatorEditResponse, ApiError>;

    async fn delete_file(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteFileResponse, ApiError>;

    async fn delete_file_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteFileEditResponse, ApiError>;

    async fn delete_fileset(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteFilesetResponse, ApiError>;

    async fn delete_fileset_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteFilesetEditResponse, ApiError>;

    async fn delete_release(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteReleaseResponse, ApiError>;

    async fn delete_release_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteReleaseEditResponse, ApiError>;

    async fn delete_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteWebcaptureResponse, ApiError>;

    async fn delete_webcapture_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteWebcaptureEditResponse, ApiError>;

    async fn delete_work(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteWorkResponse, ApiError>;

    async fn delete_work_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteWorkEditResponse, ApiError>;

    async fn get_changelog(&self, limit: Option<i64>) -> Result<GetChangelogResponse, ApiError>;

    async fn get_changelog_entry(&self, index: i64) -> Result<GetChangelogEntryResponse, ApiError>;

    async fn get_container(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetContainerResponse, ApiError>;

    async fn get_container_edit(
        &self,
        edit_id: String,
    ) -> Result<GetContainerEditResponse, ApiError>;

    async fn get_container_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetContainerHistoryResponse, ApiError>;

    async fn get_container_redirects(
        &self,
        ident: String,
    ) -> Result<GetContainerRedirectsResponse, ApiError>;

    async fn get_container_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetContainerRevisionResponse, ApiError>;

    async fn get_creator(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetCreatorResponse, ApiError>;

    async fn get_creator_edit(&self, edit_id: String) -> Result<GetCreatorEditResponse, ApiError>;

    async fn get_creator_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetCreatorHistoryResponse, ApiError>;

    async fn get_creator_redirects(
        &self,
        ident: String,
    ) -> Result<GetCreatorRedirectsResponse, ApiError>;

    async fn get_creator_releases(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetCreatorReleasesResponse, ApiError>;

    async fn get_creator_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetCreatorRevisionResponse, ApiError>;

    async fn get_editgroup(&self, editgroup_id: String) -> Result<GetEditgroupResponse, ApiError>;

    async fn get_editgroup_annotations(
        &self,
        editgroup_id: String,
        expand: Option<String>,
    ) -> Result<GetEditgroupAnnotationsResponse, ApiError>;

    async fn get_editgroups_reviewable(
        &self,
        expand: Option<String>,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
    ) -> Result<GetEditgroupsReviewableResponse, ApiError>;

    async fn get_editor(&self, editor_id: String) -> Result<GetEditorResponse, ApiError>;

    async fn get_editor_annotations(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
    ) -> Result<GetEditorAnnotationsResponse, ApiError>;

    async fn get_editor_editgroups(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
    ) -> Result<GetEditorEditgroupsResponse, ApiError>;

    async fn get_file(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFileResponse, ApiError>;

    async fn get_file_edit(&self, edit_id: String) -> Result<GetFileEditResponse, ApiError>;

    async fn get_file_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetFileHistoryResponse, ApiError>;

    async fn get_file_redirects(&self, ident: String)
        -> Result<GetFileRedirectsResponse, ApiError>;

    async fn get_file_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFileRevisionResponse, ApiError>;

    async fn get_fileset(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFilesetResponse, ApiError>;

    async fn get_fileset_edit(&self, edit_id: String) -> Result<GetFilesetEditResponse, ApiError>;

    async fn get_fileset_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetFilesetHistoryResponse, ApiError>;

    async fn get_fileset_redirects(
        &self,
        ident: String,
    ) -> Result<GetFilesetRedirectsResponse, ApiError>;

    async fn get_fileset_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFilesetRevisionResponse, ApiError>;

    async fn get_release(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetReleaseResponse, ApiError>;

    async fn get_release_edit(&self, edit_id: String) -> Result<GetReleaseEditResponse, ApiError>;

    async fn get_release_files(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetReleaseFilesResponse, ApiError>;

    async fn get_release_filesets(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetReleaseFilesetsResponse, ApiError>;

    async fn get_release_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetReleaseHistoryResponse, ApiError>;

    async fn get_release_redirects(
        &self,
        ident: String,
    ) -> Result<GetReleaseRedirectsResponse, ApiError>;

    async fn get_release_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetReleaseRevisionResponse, ApiError>;

    async fn get_release_webcaptures(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetReleaseWebcapturesResponse, ApiError>;

    async fn get_webcapture(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWebcaptureResponse, ApiError>;

    async fn get_webcapture_edit(
        &self,
        edit_id: String,
    ) -> Result<GetWebcaptureEditResponse, ApiError>;

    async fn get_webcapture_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetWebcaptureHistoryResponse, ApiError>;

    async fn get_webcapture_redirects(
        &self,
        ident: String,
    ) -> Result<GetWebcaptureRedirectsResponse, ApiError>;

    async fn get_webcapture_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWebcaptureRevisionResponse, ApiError>;

    async fn get_work(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWorkResponse, ApiError>;

    async fn get_work_edit(&self, edit_id: String) -> Result<GetWorkEditResponse, ApiError>;

    async fn get_work_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetWorkHistoryResponse, ApiError>;

    async fn get_work_redirects(&self, ident: String)
        -> Result<GetWorkRedirectsResponse, ApiError>;

    async fn get_work_releases(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetWorkReleasesResponse, ApiError>;

    async fn get_work_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWorkRevisionResponse, ApiError>;

    async fn lookup_container(
        &self,
        issnl: Option<String>,
        issne: Option<String>,
        issnp: Option<String>,
        issn: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupContainerResponse, ApiError>;

    async fn lookup_creator(
        &self,
        orcid: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupCreatorResponse, ApiError>;

    async fn lookup_editor(
        &self,
        username: Option<String>,
    ) -> Result<LookupEditorResponse, ApiError>;

    async fn lookup_file(
        &self,
        md5: Option<String>,
        sha1: Option<String>,
        sha256: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupFileResponse, ApiError>;

    async fn lookup_release(
        &self,
        doi: Option<String>,
        wikidata_qid: Option<String>,
        isbn13: Option<String>,
        pmid: Option<String>,
        pmcid: Option<String>,
        core: Option<String>,
        arxiv: Option<String>,
        jstor: Option<String>,
        ark: Option<String>,
        mag: Option<String>,
        doaj: Option<String>,
        dblp: Option<String>,
        oai: Option<String>,
        hdl: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupReleaseResponse, ApiError>;

    async fn update_container(
        &self,
        editgroup_id: String,
        ident: String,
        container_entity: models::ContainerEntity,
    ) -> Result<UpdateContainerResponse, ApiError>;

    async fn update_creator(
        &self,
        editgroup_id: String,
        ident: String,
        creator_entity: models::CreatorEntity,
    ) -> Result<UpdateCreatorResponse, ApiError>;

    async fn update_editgroup(
        &self,
        editgroup_id: String,
        editgroup: models::Editgroup,
        submit: Option<bool>,
    ) -> Result<UpdateEditgroupResponse, ApiError>;

    async fn update_editor(
        &self,
        editor_id: String,
        editor: models::Editor,
    ) -> Result<UpdateEditorResponse, ApiError>;

    async fn update_file(
        &self,
        editgroup_id: String,
        ident: String,
        file_entity: models::FileEntity,
    ) -> Result<UpdateFileResponse, ApiError>;

    async fn update_fileset(
        &self,
        editgroup_id: String,
        ident: String,
        fileset_entity: models::FilesetEntity,
    ) -> Result<UpdateFilesetResponse, ApiError>;

    async fn update_release(
        &self,
        editgroup_id: String,
        ident: String,
        release_entity: models::ReleaseEntity,
    ) -> Result<UpdateReleaseResponse, ApiError>;

    async fn update_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
        webcapture_entity: models::WebcaptureEntity,
    ) -> Result<UpdateWebcaptureResponse, ApiError>;

    async fn update_work(
        &self,
        editgroup_id: String,
        ident: String,
        work_entity: models::WorkEntity,
    ) -> Result<UpdateWorkResponse, ApiError>;
}

/// Trait to extend an API to make it easy to bind it to a context.
pub trait ContextWrapperExt<C: Send + Sync>
where
    Self: Sized,
{
    /// Binds this API to a context.
    fn with_context(self: Self, context: C) -> ContextWrapper<Self, C>;
}

impl<T: Api<C> + Send + Sync, C: Clone + Send + Sync> ContextWrapperExt<C> for T {
    fn with_context(self: T, context: C) -> ContextWrapper<T, C> {
        ContextWrapper::<T, C>::new(self, context)
    }
}

#[async_trait]
impl<T: Api<C> + Send + Sync, C: Clone + Send + Sync> ApiNoContext<C> for ContextWrapper<T, C> {
    fn poll_ready(&self, cx: &mut Context) -> Poll<Result<(), ServiceError>> {
        self.api().poll_ready(cx)
    }

    fn context(&self) -> &C {
        ContextWrapper::context(self)
    }

    async fn accept_editgroup(
        &self,
        editgroup_id: String,
    ) -> Result<AcceptEditgroupResponse, ApiError> {
        let context = self.context().clone();
        self.api().accept_editgroup(editgroup_id, &context).await
    }

    async fn auth_check(&self, role: Option<String>) -> Result<AuthCheckResponse, ApiError> {
        let context = self.context().clone();
        self.api().auth_check(role, &context).await
    }

    async fn auth_oidc(&self, auth_oidc: models::AuthOidc) -> Result<AuthOidcResponse, ApiError> {
        let context = self.context().clone();
        self.api().auth_oidc(auth_oidc, &context).await
    }

    async fn create_auth_token(
        &self,
        editor_id: String,
        duration_seconds: Option<i32>,
    ) -> Result<CreateAuthTokenResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_auth_token(editor_id, duration_seconds, &context)
            .await
    }

    async fn create_container(
        &self,
        editgroup_id: String,
        container_entity: models::ContainerEntity,
    ) -> Result<CreateContainerResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_container(editgroup_id, container_entity, &context)
            .await
    }

    async fn create_container_auto_batch(
        &self,
        container_auto_batch: models::ContainerAutoBatch,
    ) -> Result<CreateContainerAutoBatchResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_container_auto_batch(container_auto_batch, &context)
            .await
    }

    async fn create_creator(
        &self,
        editgroup_id: String,
        creator_entity: models::CreatorEntity,
    ) -> Result<CreateCreatorResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_creator(editgroup_id, creator_entity, &context)
            .await
    }

    async fn create_creator_auto_batch(
        &self,
        creator_auto_batch: models::CreatorAutoBatch,
    ) -> Result<CreateCreatorAutoBatchResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_creator_auto_batch(creator_auto_batch, &context)
            .await
    }

    async fn create_editgroup(
        &self,
        editgroup: models::Editgroup,
    ) -> Result<CreateEditgroupResponse, ApiError> {
        let context = self.context().clone();
        self.api().create_editgroup(editgroup, &context).await
    }

    async fn create_editgroup_annotation(
        &self,
        editgroup_id: String,
        editgroup_annotation: models::EditgroupAnnotation,
    ) -> Result<CreateEditgroupAnnotationResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_editgroup_annotation(editgroup_id, editgroup_annotation, &context)
            .await
    }

    async fn create_file(
        &self,
        editgroup_id: String,
        file_entity: models::FileEntity,
    ) -> Result<CreateFileResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_file(editgroup_id, file_entity, &context)
            .await
    }

    async fn create_file_auto_batch(
        &self,
        file_auto_batch: models::FileAutoBatch,
    ) -> Result<CreateFileAutoBatchResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_file_auto_batch(file_auto_batch, &context)
            .await
    }

    async fn create_fileset(
        &self,
        editgroup_id: String,
        fileset_entity: models::FilesetEntity,
    ) -> Result<CreateFilesetResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_fileset(editgroup_id, fileset_entity, &context)
            .await
    }

    async fn create_fileset_auto_batch(
        &self,
        fileset_auto_batch: models::FilesetAutoBatch,
    ) -> Result<CreateFilesetAutoBatchResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_fileset_auto_batch(fileset_auto_batch, &context)
            .await
    }

    async fn create_release(
        &self,
        editgroup_id: String,
        release_entity: models::ReleaseEntity,
    ) -> Result<CreateReleaseResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_release(editgroup_id, release_entity, &context)
            .await
    }

    async fn create_release_auto_batch(
        &self,
        release_auto_batch: models::ReleaseAutoBatch,
    ) -> Result<CreateReleaseAutoBatchResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_release_auto_batch(release_auto_batch, &context)
            .await
    }

    async fn create_webcapture(
        &self,
        editgroup_id: String,
        webcapture_entity: models::WebcaptureEntity,
    ) -> Result<CreateWebcaptureResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_webcapture(editgroup_id, webcapture_entity, &context)
            .await
    }

    async fn create_webcapture_auto_batch(
        &self,
        webcapture_auto_batch: models::WebcaptureAutoBatch,
    ) -> Result<CreateWebcaptureAutoBatchResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_webcapture_auto_batch(webcapture_auto_batch, &context)
            .await
    }

    async fn create_work(
        &self,
        editgroup_id: String,
        work_entity: models::WorkEntity,
    ) -> Result<CreateWorkResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_work(editgroup_id, work_entity, &context)
            .await
    }

    async fn create_work_auto_batch(
        &self,
        work_auto_batch: models::WorkAutoBatch,
    ) -> Result<CreateWorkAutoBatchResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .create_work_auto_batch(work_auto_batch, &context)
            .await
    }

    async fn delete_container(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteContainerResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_container(editgroup_id, ident, &context)
            .await
    }

    async fn delete_container_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteContainerEditResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_container_edit(editgroup_id, edit_id, &context)
            .await
    }

    async fn delete_creator(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteCreatorResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_creator(editgroup_id, ident, &context)
            .await
    }

    async fn delete_creator_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteCreatorEditResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_creator_edit(editgroup_id, edit_id, &context)
            .await
    }

    async fn delete_file(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteFileResponse, ApiError> {
        let context = self.context().clone();
        self.api().delete_file(editgroup_id, ident, &context).await
    }

    async fn delete_file_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteFileEditResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_file_edit(editgroup_id, edit_id, &context)
            .await
    }

    async fn delete_fileset(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteFilesetResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_fileset(editgroup_id, ident, &context)
            .await
    }

    async fn delete_fileset_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteFilesetEditResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_fileset_edit(editgroup_id, edit_id, &context)
            .await
    }

    async fn delete_release(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteReleaseResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_release(editgroup_id, ident, &context)
            .await
    }

    async fn delete_release_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteReleaseEditResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_release_edit(editgroup_id, edit_id, &context)
            .await
    }

    async fn delete_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteWebcaptureResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_webcapture(editgroup_id, ident, &context)
            .await
    }

    async fn delete_webcapture_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteWebcaptureEditResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_webcapture_edit(editgroup_id, edit_id, &context)
            .await
    }

    async fn delete_work(
        &self,
        editgroup_id: String,
        ident: String,
    ) -> Result<DeleteWorkResponse, ApiError> {
        let context = self.context().clone();
        self.api().delete_work(editgroup_id, ident, &context).await
    }

    async fn delete_work_edit(
        &self,
        editgroup_id: String,
        edit_id: String,
    ) -> Result<DeleteWorkEditResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .delete_work_edit(editgroup_id, edit_id, &context)
            .await
    }

    async fn get_changelog(&self, limit: Option<i64>) -> Result<GetChangelogResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_changelog(limit, &context).await
    }

    async fn get_changelog_entry(&self, index: i64) -> Result<GetChangelogEntryResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_changelog_entry(index, &context).await
    }

    async fn get_container(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetContainerResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_container(ident, expand, hide, &context)
            .await
    }

    async fn get_container_edit(
        &self,
        edit_id: String,
    ) -> Result<GetContainerEditResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_container_edit(edit_id, &context).await
    }

    async fn get_container_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetContainerHistoryResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_container_history(ident, limit, &context)
            .await
    }

    async fn get_container_redirects(
        &self,
        ident: String,
    ) -> Result<GetContainerRedirectsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_container_redirects(ident, &context).await
    }

    async fn get_container_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetContainerRevisionResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_container_revision(rev_id, expand, hide, &context)
            .await
    }

    async fn get_creator(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetCreatorResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_creator(ident, expand, hide, &context).await
    }

    async fn get_creator_edit(&self, edit_id: String) -> Result<GetCreatorEditResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_creator_edit(edit_id, &context).await
    }

    async fn get_creator_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetCreatorHistoryResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_creator_history(ident, limit, &context).await
    }

    async fn get_creator_redirects(
        &self,
        ident: String,
    ) -> Result<GetCreatorRedirectsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_creator_redirects(ident, &context).await
    }

    async fn get_creator_releases(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetCreatorReleasesResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_creator_releases(ident, hide, &context).await
    }

    async fn get_creator_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetCreatorRevisionResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_creator_revision(rev_id, expand, hide, &context)
            .await
    }

    async fn get_editgroup(&self, editgroup_id: String) -> Result<GetEditgroupResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_editgroup(editgroup_id, &context).await
    }

    async fn get_editgroup_annotations(
        &self,
        editgroup_id: String,
        expand: Option<String>,
    ) -> Result<GetEditgroupAnnotationsResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_editgroup_annotations(editgroup_id, expand, &context)
            .await
    }

    async fn get_editgroups_reviewable(
        &self,
        expand: Option<String>,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
    ) -> Result<GetEditgroupsReviewableResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_editgroups_reviewable(expand, limit, before, since, &context)
            .await
    }

    async fn get_editor(&self, editor_id: String) -> Result<GetEditorResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_editor(editor_id, &context).await
    }

    async fn get_editor_annotations(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
    ) -> Result<GetEditorAnnotationsResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_editor_annotations(editor_id, limit, before, since, &context)
            .await
    }

    async fn get_editor_editgroups(
        &self,
        editor_id: String,
        limit: Option<i64>,
        before: Option<chrono::DateTime<chrono::Utc>>,
        since: Option<chrono::DateTime<chrono::Utc>>,
    ) -> Result<GetEditorEditgroupsResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_editor_editgroups(editor_id, limit, before, since, &context)
            .await
    }

    async fn get_file(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFileResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_file(ident, expand, hide, &context).await
    }

    async fn get_file_edit(&self, edit_id: String) -> Result<GetFileEditResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_file_edit(edit_id, &context).await
    }

    async fn get_file_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetFileHistoryResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_file_history(ident, limit, &context).await
    }

    async fn get_file_redirects(
        &self,
        ident: String,
    ) -> Result<GetFileRedirectsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_file_redirects(ident, &context).await
    }

    async fn get_file_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFileRevisionResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_file_revision(rev_id, expand, hide, &context)
            .await
    }

    async fn get_fileset(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFilesetResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_fileset(ident, expand, hide, &context).await
    }

    async fn get_fileset_edit(&self, edit_id: String) -> Result<GetFilesetEditResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_fileset_edit(edit_id, &context).await
    }

    async fn get_fileset_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetFilesetHistoryResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_fileset_history(ident, limit, &context).await
    }

    async fn get_fileset_redirects(
        &self,
        ident: String,
    ) -> Result<GetFilesetRedirectsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_fileset_redirects(ident, &context).await
    }

    async fn get_fileset_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetFilesetRevisionResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_fileset_revision(rev_id, expand, hide, &context)
            .await
    }

    async fn get_release(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetReleaseResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_release(ident, expand, hide, &context).await
    }

    async fn get_release_edit(&self, edit_id: String) -> Result<GetReleaseEditResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_release_edit(edit_id, &context).await
    }

    async fn get_release_files(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetReleaseFilesResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_release_files(ident, hide, &context).await
    }

    async fn get_release_filesets(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetReleaseFilesetsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_release_filesets(ident, hide, &context).await
    }

    async fn get_release_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetReleaseHistoryResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_release_history(ident, limit, &context).await
    }

    async fn get_release_redirects(
        &self,
        ident: String,
    ) -> Result<GetReleaseRedirectsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_release_redirects(ident, &context).await
    }

    async fn get_release_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetReleaseRevisionResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_release_revision(rev_id, expand, hide, &context)
            .await
    }

    async fn get_release_webcaptures(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetReleaseWebcapturesResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_release_webcaptures(ident, hide, &context)
            .await
    }

    async fn get_webcapture(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWebcaptureResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_webcapture(ident, expand, hide, &context)
            .await
    }

    async fn get_webcapture_edit(
        &self,
        edit_id: String,
    ) -> Result<GetWebcaptureEditResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_webcapture_edit(edit_id, &context).await
    }

    async fn get_webcapture_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetWebcaptureHistoryResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_webcapture_history(ident, limit, &context)
            .await
    }

    async fn get_webcapture_redirects(
        &self,
        ident: String,
    ) -> Result<GetWebcaptureRedirectsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_webcapture_redirects(ident, &context).await
    }

    async fn get_webcapture_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWebcaptureRevisionResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_webcapture_revision(rev_id, expand, hide, &context)
            .await
    }

    async fn get_work(
        &self,
        ident: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWorkResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_work(ident, expand, hide, &context).await
    }

    async fn get_work_edit(&self, edit_id: String) -> Result<GetWorkEditResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_work_edit(edit_id, &context).await
    }

    async fn get_work_history(
        &self,
        ident: String,
        limit: Option<i64>,
    ) -> Result<GetWorkHistoryResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_work_history(ident, limit, &context).await
    }

    async fn get_work_redirects(
        &self,
        ident: String,
    ) -> Result<GetWorkRedirectsResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_work_redirects(ident, &context).await
    }

    async fn get_work_releases(
        &self,
        ident: String,
        hide: Option<String>,
    ) -> Result<GetWorkReleasesResponse, ApiError> {
        let context = self.context().clone();
        self.api().get_work_releases(ident, hide, &context).await
    }

    async fn get_work_revision(
        &self,
        rev_id: String,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<GetWorkRevisionResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .get_work_revision(rev_id, expand, hide, &context)
            .await
    }

    async fn lookup_container(
        &self,
        issnl: Option<String>,
        issne: Option<String>,
        issnp: Option<String>,
        issn: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupContainerResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .lookup_container(
                issnl,
                issne,
                issnp,
                issn,
                wikidata_qid,
                expand,
                hide,
                &context,
            )
            .await
    }

    async fn lookup_creator(
        &self,
        orcid: Option<String>,
        wikidata_qid: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupCreatorResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .lookup_creator(orcid, wikidata_qid, expand, hide, &context)
            .await
    }

    async fn lookup_editor(
        &self,
        username: Option<String>,
    ) -> Result<LookupEditorResponse, ApiError> {
        let context = self.context().clone();
        self.api().lookup_editor(username, &context).await
    }

    async fn lookup_file(
        &self,
        md5: Option<String>,
        sha1: Option<String>,
        sha256: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupFileResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .lookup_file(md5, sha1, sha256, expand, hide, &context)
            .await
    }

    async fn lookup_release(
        &self,
        doi: Option<String>,
        wikidata_qid: Option<String>,
        isbn13: Option<String>,
        pmid: Option<String>,
        pmcid: Option<String>,
        core: Option<String>,
        arxiv: Option<String>,
        jstor: Option<String>,
        ark: Option<String>,
        mag: Option<String>,
        doaj: Option<String>,
        dblp: Option<String>,
        oai: Option<String>,
        hdl: Option<String>,
        expand: Option<String>,
        hide: Option<String>,
    ) -> Result<LookupReleaseResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .lookup_release(
                doi,
                wikidata_qid,
                isbn13,
                pmid,
                pmcid,
                core,
                arxiv,
                jstor,
                ark,
                mag,
                doaj,
                dblp,
                oai,
                hdl,
                expand,
                hide,
                &context,
            )
            .await
    }

    async fn update_container(
        &self,
        editgroup_id: String,
        ident: String,
        container_entity: models::ContainerEntity,
    ) -> Result<UpdateContainerResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_container(editgroup_id, ident, container_entity, &context)
            .await
    }

    async fn update_creator(
        &self,
        editgroup_id: String,
        ident: String,
        creator_entity: models::CreatorEntity,
    ) -> Result<UpdateCreatorResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_creator(editgroup_id, ident, creator_entity, &context)
            .await
    }

    async fn update_editgroup(
        &self,
        editgroup_id: String,
        editgroup: models::Editgroup,
        submit: Option<bool>,
    ) -> Result<UpdateEditgroupResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_editgroup(editgroup_id, editgroup, submit, &context)
            .await
    }

    async fn update_editor(
        &self,
        editor_id: String,
        editor: models::Editor,
    ) -> Result<UpdateEditorResponse, ApiError> {
        let context = self.context().clone();
        self.api().update_editor(editor_id, editor, &context).await
    }

    async fn update_file(
        &self,
        editgroup_id: String,
        ident: String,
        file_entity: models::FileEntity,
    ) -> Result<UpdateFileResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_file(editgroup_id, ident, file_entity, &context)
            .await
    }

    async fn update_fileset(
        &self,
        editgroup_id: String,
        ident: String,
        fileset_entity: models::FilesetEntity,
    ) -> Result<UpdateFilesetResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_fileset(editgroup_id, ident, fileset_entity, &context)
            .await
    }

    async fn update_release(
        &self,
        editgroup_id: String,
        ident: String,
        release_entity: models::ReleaseEntity,
    ) -> Result<UpdateReleaseResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_release(editgroup_id, ident, release_entity, &context)
            .await
    }

    async fn update_webcapture(
        &self,
        editgroup_id: String,
        ident: String,
        webcapture_entity: models::WebcaptureEntity,
    ) -> Result<UpdateWebcaptureResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_webcapture(editgroup_id, ident, webcapture_entity, &context)
            .await
    }

    async fn update_work(
        &self,
        editgroup_id: String,
        ident: String,
        work_entity: models::WorkEntity,
    ) -> Result<UpdateWorkResponse, ApiError> {
        let context = self.context().clone();
        self.api()
            .update_work(editgroup_id, ident, work_entity, &context)
            .await
    }
}

#[cfg(feature = "client")]
pub mod client;

// Re-export Client as a top-level name
#[cfg(feature = "client")]
pub use client::Client;

#[cfg(feature = "server")]
pub mod server;

// Re-export router() as a top-level name
#[cfg(feature = "server")]
pub use self::server::Service;

#[cfg(feature = "server")]
pub mod context;

pub mod models;

#[cfg(any(feature = "client", feature = "server"))]
pub(crate) mod header;
