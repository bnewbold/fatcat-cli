
.PHONY: help
help: ## Print info about all commands
	@echo "Commands:"
	@echo
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "    \033[01;32m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: build ## Run all tests
	cargo test
	./tests/live.sh

.PHONY: lint
lint: ## Run syntax/style checks
	cargo clippy -p fatcat-cli -- --no-deps

.PHONY: fmt
fmt: ## Run syntax re-formatting
	cargo fmt -p fatcat-cli

.PHONY: build
build: ## Build
	cargo build

.PHONY: build-release
build-release: ## Build for release
	cargo build --release

.PHONY: completions
completions: build  ## generate shell completions
	./target/debug/fatcat-cli --shell-completions bash status > extra/fatcat-cli.bash_completions
	./target/debug/fatcat-cli --shell-completions bash status > extra/fatcat-cli.zsh_completions

extra/fatcat-cli.1: extra/fatcat-cli.1.scdoc
	scdoc < extra/fatcat-cli.1.scdoc > extra/fatcat-cli.1

.PHONY: manpage
manpage: extra/fatcat-cli.1  ## Rebuild manpage using scdoc

.PHONY: deb
deb: ## Build debian packages (.deb files)
	cargo deb -p fatcat-cli

.PHONY: homebrew-folder
homebrew-folder:  extra/fatcat-cli.1 completions build-release ## Copy files into homebrew folder
	mkdir -p target/homebrew/doc
	cp extra/fatcat-cli.1 target/homebrew/doc/fatcat-cli.1
	mkdir -p target/homebrew/complete
	cp extra/fatcat-cli.bash_completions target/homebrew/complete/fatcat-cli.bash
	cp extra/fatcat-cli.zsh_completions target/homebrew/complete/_fatcat-cli
	cp target/release/fatcat-cli target/homebrew/fatcat-cli
