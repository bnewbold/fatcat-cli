#!/bin/bash

set -e -u -o pipefail

export FATCAT=./target/debug/fatcat-cli

$FATCAT search releases "metadata author:phillips" > /dev/null

$FATCAT get doi:10.1002/spe.659 > /dev/null

$FATCAT search releases journal:"first monday" --entity-json --expand files | $FATCAT batch --limit 1 download > /dev/null
